using System;
using Microsoft.SPOT;

namespace MosziNet.HomeAutomation.Logging
{
    public enum LogLevel
    {
        Debug,
        Error,
        Information
    }
}
