using System;
using Microsoft.SPOT;

namespace MosziNet.HomeAutomation.Messaging
{
    /// <summary>
    /// Describes a message that is posted by devices, sensors, or other entities in the system.
    /// </summary>
    public interface IMessage
    {
        
    }
}
