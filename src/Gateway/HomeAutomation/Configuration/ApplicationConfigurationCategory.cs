using System;
using Microsoft.SPOT;

namespace MosziNet.HomeAutomation.Configuration
{
    public static class ApplicationConfigurationCategory
    {
        public static readonly string DeviceTypeID = "DevicTypeID";

        public static readonly string XBeeFrameProcessor = "XBeeFrameProcessor";
    }
}
