# HomeAutomation
A home automation project using XBee, Netduino, Raspberry PI 2 and other devices.
## Introduction
This repository contains information on how to build a home automation system based on XBee radios, Netduino and Raspberry PI 2 boards and other devices. 
It documents each device that is used in the system with schematics and  provides a system architecture that is easy to build at home. 

## Where to find additional information
Please browse the Wiki pages, the project will continue to be documented there. The wiki pages can be found here: [https://github.com/moszinet/HomeAutomation/wiki](https://github.com/moszinet/HomeAutomation/wiki) 
